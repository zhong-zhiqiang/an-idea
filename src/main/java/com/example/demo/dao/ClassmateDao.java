package com.example.demo.dao;

import com.example.demo.polo.Classmate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClassmateDao extends JpaRepository<Classmate,Integer> {


}
