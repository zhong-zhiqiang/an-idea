package com.example.demo.controller;

import com.example.demo.polo.Classmate;
import com.example.demo.polo.Result;
import com.example.demo.service.ClassmateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClassmateController {
    @Autowired
    ClassmateService classmateService;
    @PostMapping("/app3/classmate/list")
    public Result getClassmateList(){
        List<Classmate>list=classmateService.getClassmateList();
        Result r=new Result();
        if (list!=null){
            r.setMsg("成功");
            r.setSuccesss(true);
            r.setList(list);
        }else {
            r.setMsg("失败");
            r.setSuccesss(false);
        }
        return r;

    }


}
