package com.example.demo.service;

import com.example.demo.dao.ClassmateDao;
import com.example.demo.polo.Classmate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassmateService {
    //查询所有学生信息
    @Autowired
    ClassmateDao classmateDao;
    public List<Classmate> getClassmateList(){
        return classmateDao.findAll();
    }
}
