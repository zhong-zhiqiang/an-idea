package com.example.demo.polo;

import java.util.List;

public class Result {
    private boolean successs;
    private String msg;
    private List<Classmate> list;

    public boolean isSuccesss() {
        return successs;
    }

    public void setSuccesss(boolean successs) {
        this.successs = successs;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Classmate> getList() {
        return list;
    }

    public void setList(List<Classmate> list) {
        this.list = list;
    }
}
